//
// Created by argo on 24/12/18.
//

#include <Log/Log.hpp>
#include <Core/DiscretiseTransform.hpp>
#include <Core/DistanceHaussdorf.hpp>
#include "Test.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <glm/vec2.hpp>
#include <Core/Canny.hpp>
#include <Core/Search.hpp>

using namespace cv;
using namespace std;

Test::Test()
{

    Log(logDebug) << "Test ...";


    //testDiscretiseTransform();
    //testTuples();
    //testDistanceHaussdorff();

    Log(logDebug) << "End Test";
}

int Test::testDiscretiseTransform()
{
    //Log(logDebug) << "Test discretiseTransform";

    cv::Mat image;
    image = cv::imread("../../img/lena_grayscale.png" , 0);   // Read the file

    if(! image.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    /*DiscretiseTransform dt(512,512,512,512);

    int i = 0;

    while(dt.hasNext()){
        dt.getItr();

        if(i%1000000000==0 && i!=0) {
            dt.displayT();
        }

        i++;
    }*/

    return 0;
}

void Test::testTuples() {
    Tuples t;
    t.test();
}

void Test::testDistanceHaussdorff() {
    Log(logInfo) << "Test distance Haussdorff";

    VectorPoint2 pts1;
    pts1.push_back(Vector2(0,0));
    pts1.push_back(Vector2(4,2));

    VectorPoint2 pts2;
    pts2.push_back(Vector2(0,0));
    pts2.push_back(Vector2(4,3));

    VectorPoint2 pts3;
    pts3.push_back(Vector2(4,2));
    pts3.push_back(Vector2(10,2));
    pts3.push_back(Vector2(2,2));

    /*Log(logInfo) << "Test fonction Pierre";
    Log(logInfo) << "(Expect:1) " << hausdorffDirected(pts1,pts2);
    Log(logInfo) << "(Expect:1) " << hausdorffDirected(pts2,pts1);
    Log(logInfo) << "(Expect:2.8284) " << hausdorffDirected(pts1,pts3);
    Log(logInfo) << "(Expect:6) " << hausdorffDirected(pts3,pts1);

    Log(logInfo) << "Test d1";
    Log(logInfo) << "(Expect:0) " << d1(pts1,pts2);
    Log(logInfo) << "(Expect:0) " << d1(pts2,pts1);
    Log(logInfo) << "(Expect:0) " << d1(pts1,pts3);
    Log(logInfo) << "(Expect:0) " << d1(pts3,pts1);

    Log(logInfo) << "Test d2";
    Log(logInfo) << "(Expect:0) " << d2(pts1,pts2);
    Log(logInfo) << "(Expect:0) " << d2(pts2,pts1);
    Log(logInfo) << "(Expect:0) " << d2(pts1,pts3);
    Log(logInfo) << "(Expect:2) " << d2(pts3,pts1);

    Log(logInfo) << "Test d3";
    Log(logInfo) << "(Expect:1) " << d3(pts1,pts2);
    Log(logInfo) << "(Expect:1) " << d3(pts2,pts1);
    Log(logInfo) << "(Expect:2.82843) " << d3(pts1,pts3);
    Log(logInfo) << "(Expect:2) " << d3(pts3,pts1);

    Log(logInfo) << "Test d4";
    Log(logInfo) << "(Expect:1) " << d4(pts1,pts2);
    Log(logInfo) << "(Expect:1) " << d4(pts2,pts1);
    Log(logInfo) << "(Expect:2.82843) " << d4(pts1,pts3);
    Log(logInfo) << "(Expect:6) " << d4(pts3,pts1);

    Log(logInfo) << "Test d5";
    Log(logInfo) << "(Expect:1) " << d5(pts1,pts2);
    Log(logInfo) << "(Expect:1) " << d5(pts2,pts1);
    Log(logInfo) << "(Expect:2.8284) " << d5(pts1,pts3);
    Log(logInfo) << "(Expect:6) " << d5(pts3,pts1);

    Log(logInfo) << "Test d6";
    Log(logInfo) << "(Expect:0.5) " << d6(pts1,pts2);
    Log(logInfo) << "(Expect:0.5) " << d6(pts2,pts1);
    Log(logInfo) << "(Expect:1.41421) " << d6(pts1,pts3);
    Log(logInfo) << "(Expect:2.66667) " << d6(pts3,pts1); */

    Log(logInfo) << "End test distance Haussdorff";
}