//
// Created by pierre on 27/12/18.
//

#include "AffineTransform.hpp"


Vector2 transformPoint(Vector2 point, Matrix2 mat, Vector2 trans){
    return mat * point + trans;
}

Vector2 transformPoint2(Vector2 point, Tuples tuples){
    return tuples.getRot() * point + tuples.getTrans();
}

Mat transformImage(Mat mod, Mat img, Tuples transform){
    Mat res;

    res.create(img.rows, img.cols, img.type());
    res.zeros(img.rows,img.cols, img.type());
    res.setTo(0);
    Vector2 p;

    for(int i=0;i<mod.rows;i++)
    {
        for (int j=0;j<mod.cols;j++)
        {
            p=transformPoint2(Vector2((float)i,(float)j), transform);
            if(p.x>=0 && p.x<img.rows && p.y>=0 && p.y<img.cols)
                res.at<uchar>(p.x,p.y) = mod.at<uchar>(i,j);

        }
    }
    return res;
}


VectorPoint2 transformContours(VectorPoint2 contours, Mat img, Tuples transform, bool &out){
    VectorPoint2 res;
    Vector2 p;
    out = false;
    for(auto i:contours){
        p=transformPoint2(Vector2(i.x,i.y), transform);
        if(p.x>=0 && p.x<img.rows && p.y>=0 && p.y<img.cols) {
            res.push_back(p);
        }else{
            out = true;
            return res;
        }
    }
    return res;
}


VectorPoint2 transformContoursMinMax(VectorPoint2 contours, Mat mod, Mat img, Tuples transform, Box &box, bool &out) {
    Vector2 a=transformPoint2(Vector2(0,0), transform);
    Vector2 b=transformPoint2(Vector2(0,mod.cols), transform);
    Vector2 c=transformPoint2(Vector2(mod.rows,0), transform);
    Vector2 d=transformPoint2(Vector2(mod.rows,mod.cols), transform);

    box.xMax= std::max(std::max(0,(int)a.x), (int)std::max(std::max(b.x,c.x),d.x));
    box.yMax= std::max(std::max(0,(int)a.y), (int)std::max(std::max(b.y,c.y),d.y));
    box.xMin= std::min(std::min(img.rows,(int)a.x), (int)std::min(std::min(b.x,c.x),d.x));
    box.yMin= std::min(std::min(img.cols,(int)a.y), (int)std::min(std::min(b.y,c.y),d.y));

    return transformContours(contours, img, transform, out);
}

Mat getImgFromContours(VectorPoint2 contours, Mat img){
    Mat res;

    res.create(img.rows, img.cols, img.type());
    res.zeros(img.rows,img.cols, img.type());
    res.setTo(0);

    for(auto i:contours){
        if(i.x>=0 && i.x<img.rows && i.y>=0 && i.y<img.cols)
            res.at<uchar>(i.x,i.y) = 255;
    }
    return res;
}
