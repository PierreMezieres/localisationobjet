//
// Created by argo on 24/12/18.
//

#ifndef PROJECT_DISCRETISETRANSFORM_HPP
#define PROJECT_DISCRETISETRANSFORM_HPP

#include "Macros.hpp"
#include "Tuples.hpp"
#include "DistanceHaussdorf.hpp"


typedef std::pair<Tuples,Tuples> TP;


class DiscretiseTransform {
public:

    DiscretiseTransform(Mat _img, Mat mod, std::vector<Point2> _contours, double dmin, double dmax, double skewmax, double smax, bool trans_,int _methodSearch, DistanceHausdorff *distanceHausdorff_,
            double Am_, double AM_, double Bm_, double BM_, double Cm_, double CM_, double Dm_, double dM_);


    void begin();
    bool hasNext();
    Tuples getItr();

    void displayT();

    void setTf(double v);
    void setFf(double v);


private:

    bool check_cell(TP tp);
    std::vector<TP> subdivide(TP tp);
    double interesting_cell(TP tp, int level);
    void beginTrans();
    bool hasNextTrans();

    int xMax, yMax, xMod, yMod;
    double d_min,d_max,skew_max,s_max;
    bool trans;
    double Am,AM,Bm,BM,Cm,CM,Dm,DM;

    bool test_skew(Tuples t);
    bool test_s(Tuples t);
    bool test_area(Tuples t);
    bool all_test(Tuples t);

    /*bool test1();
    bool test2();
    bool all_trans_done = false;*/

    Tuples e,t,init;

    int methodSearch;



//    std::vector<TP> listLevelPlusOne;
//    std::vector<TP> listLevel;
    std::vector<std::vector<TP>> listClimbing;
    DistanceHausdorff *distanceHausdorff;
    bool stop = false;
    double eps;
    std::vector<Point2> contours;
    double tf, ff;
    Mat img;
};


#endif //PROJECT_DISCRETISETRANSFORM_HPP
