//
// Created by pierre on 27/12/18.
//

#ifndef PROJECT_AFFINETRANSFORM_HPP
#define PROJECT_AFFINETRANSFORM_HPP


#include "Macros.hpp"
#include "Tuples.hpp"

Vector2 transformPoint(Vector2 point, Matrix2 mat, Vector2 trans);
Vector2 transformPoint2(Vector2 point, Tuples tuples);
Mat transformImage(Mat mod, Mat img, Tuples transform);


 VectorPoint2 transformContours(VectorPoint2 contours, Mat img, Tuples transform, bool &out);
 VectorPoint2 transformContoursMinMax(VectorPoint2 contours, Mat mod, Mat img, Tuples transform, Box &box, bool &out);

 Mat getImgFromContours(VectorPoint2 contours, Mat img);


#endif //PROJECT_AFFINETRANSFORM_HPP
