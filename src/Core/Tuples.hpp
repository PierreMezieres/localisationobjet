//
// Created by pierre on 27/12/18.
//

#ifndef PROJECT_TUPLES_HPP
#define PROJECT_TUPLES_HPP


#include "Macros.hpp"

class Tuples {
public:
    Tuples(float _a, float _b, float _c, float _d, float _x, float _y);
    Tuples();

    Matrix2 getRot();
    Vector2 getTrans();

    void addA(float v);
    void addB(float v);
    void addC(float v);
    void addD(float v);
    void addX(float v);
    void addY(float v);

    float a,b,c,d,x,y;

    void test();
    void display();
};


#endif //PROJECT_TUPLES_HPP
