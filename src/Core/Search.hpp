//
// Created by pierre on 28/12/18.
//

#ifndef PROJECT_SEARCH_HPP
#define PROJECT_SEARCH_HPP


#include "Macros.hpp"
#include "DistanceHaussdorf.hpp"

class Search {
public:
    Search();

    void launchSearch(int _distD, int _distd, int _nbValue); //nbValue valeur au cas où on utilise dnb

    void setImage(std::string name,Mat i);
    void setModel(Mat i);
    void setMethod(int method);

    void setThresholdCannyModel(int v);
    void setThresholdCannyImage(int v);
    void setAreaMin(double v);
    void setAreaMax(double v);
    void setSkewMax(double v);
    void setSMax(double v);
    void setTf(double v);
    void setTr(double v);
    void setFf(double v);
    void setFr(double v);
    void setpMul(double v);
    void setpPlu(double v);
    void setTrans(bool b);
    void setFactorImage(float f);
    void setFactorResult(float f);

    void setAMin(double v);
    void setAMax(double v);
    void setBMin(double v);
    void setBMax(double v);
    void setCMin(double v);
    void setCMax(double v);
    void setDMin(double v);
    void setDMax(double v);
    void setAll(double am, double aM, double bm, double bM, double cm, double cM, double dm, double dM);

    void afficheImage();
    void afficheModel();
    void afficheImageContour();
    void afficheModelContour();
    void afficheResultat();


private:

    bool verifParam();
    void launchSearchFirstThreshold();
    void launchSearchBestSolutionFf();
    void launchSearchBestSolutionTf();
    void launchSearchFirstThresholdWithReverse();
    void launchSearchBestSolutionFfWithReverse();
    void launchSearchBestSolutionTfWithReverse();


    std::string name_img;
    Mat image;
    Mat image_contour;
    VectorPoint2 contoursImg;

    Mat model;
    Mat model_contour;
    VectorPoint2 contoursMod;

    Mat result;
    Mat img_from_result;

    DistanceHausdorff *distanceHausdorff;

    // DO NOT CHANGE THOSE VALUES
    float factor_image = 0.3;
    float factor_result = 1.0;
    bool trans = true;
    int methodSearch = 0;
    int threshold_canny_model = 100;
    int threshold_canny_image = 100;
    int distD, distd;
    int nbValue;

    double d_min = 0.5,d_max = 1,skew_max = 2,s_max= 0.5, tf=5, tr=5, ff=0.5, fr=0.5, pMul =1, pPlu=0;
    double aMin = -1, aMax = 1, bMin = -1, bMax = 1, cMin = -1, cMax = 1, dMin = -1, dMax = 1;

};


#endif //PROJECT_SEARCH_HPP
