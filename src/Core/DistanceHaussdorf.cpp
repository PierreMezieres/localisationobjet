//
// Created by pierre on 27/12/18.
//

#include <Log/Log.hpp>
#include "DistanceHaussdorf.hpp"

DistanceHausdorff::DistanceHausdorff() {
}

void DistanceHausdorff::setImg(std::string name, int threshold_canny_, Mat img_, VectorPoint2 contour_img_) {
    if(id!=name || threshold_canny_!=threshold_canny){
        Log(logInfo) << "Image changed ...";
        id = name;
        threshold_canny=threshold_canny_;
        img = img_;
        contour_img = contour_img_;
        init();
    }

}

/* ************* INIT *********** */

void DistanceHausdorff::init() {
    for(auto i:deltaXY) i.clear();
    deltaXY.clear();
    std::vector<double> vec;
    double max = INF;
    for(int i = 0; i<img.cols; i++){
        vec.clear();
        for(int j = 0; j<img.rows; j++){
            max = INF;
            for(auto k : contour_img){
                double dist = glm::distance(k,Point2(i,j));
                if(dist<max) max = dist;
            }
            vec.push_back(max);
        }
        deltaXY.push_back(vec);
    }
}


double DistanceHausdorff::forward(VectorPoint2 element) {
    double max = 0;
    for(auto i:element){
        double dist = deltaXY[i.x][i.y];
        if(dist>max) max=dist;
    }
    return max;
}

double DistanceHausdorff::forward_tf(VectorPoint2 element, double threshold) {
    double nbElem = 0;
    for(auto i:element){
        double dist = deltaXY[i.x][i.y];
        if(dist < threshold) nbElem++;
    }
    return nbElem / element.size();
}

double DistanceHausdorff::forward_ff(VectorPoint2 element, double threshold) {
    VectorDouble vec;
    for(auto i:element){
        vec.push_back(deltaXY[i.x][i.y]);
    }
    std::sort(vec.begin(), vec.end());
    int index = int(std::round(vec.size() * threshold));
    return vec[index-1];
}

double DistanceHausdorff::forward_tf_prim(VectorPoint2 element, double threshold, int level){
    VectorDouble vec;
    if(deltaPrims.size()==0){
        Log(logError) << "Delta prims empty ...";
        return INF;
    }
    if(element.size()==0) return INF;
    std::vector<std::vector<double>> mat = deltaPrims[level];
    double nbElem = 0;
    for(auto i:element){
        double dist = mat[i.x][i.y];
        if(dist < threshold) nbElem++;
    }
    return nbElem / element.size();
}

double DistanceHausdorff::forward_ff_prim(VectorPoint2 element, double threshold, int level) {
    VectorDouble vec;
    if(deltaPrims.size()==0){
        Log(logError) << "Delta prims empty ...";
        return INF;
    }
    if(element.size()==0) return INF;

    std::vector<std::vector<double>> mat = deltaPrims[level];
    for(auto i:element){
        vec.push_back(mat[i.x][i.y]);
    }
    std::sort(vec.begin(), vec.end());
    int index = int(std::round(vec.size() * threshold));
    return vec[index-1];
}

double DistanceHausdorff::reverse_tf(VectorPoint2 img, VectorPoint2 elt, double threshold, Box box) {
    double nbElem = 0;
    double elem = 0;

    for(auto i:img){
        if(i.x>box.xMin && i.x<box.xMax && i.y>box.yMin && i.y<box.yMax){
            for(auto j:elt){
                double dist = glm::distance(i,j);
                if(dist<threshold) nbElem++;
            }
            elem++;
        }
    }
    return nbElem / elem;
}

double DistanceHausdorff::reverse_ff(VectorPoint2 img, VectorPoint2 elt, double threshold, Box box) {
    VectorDouble vec;

    for(auto i:img){
        if(i.x>box.xMin && i.x<box.xMax && i.y>box.yMin && i.y<box.yMax){
            for(auto j:elt){
                vec.push_back(glm::distance(i,j));
            }
        }
    }
    std::sort(vec.begin(), vec.end());
    int index = int(std::round(vec.size() * threshold));
    return vec[index-1];
}

double DistanceHausdorff::reverse_max(VectorPoint2 img, VectorPoint2 elt) {
    double minimum = INF;
    double maximum = -1;
    for(auto i:elt){
        minimum = INF;
        for(auto j:img){
            double dist = glm::distance(i,j);
            if(dist<minimum) minimum=dist;
        }
        if(minimum>maximum) maximum=minimum;
    }
    if (maximum == -1) return INF;
    return maximum;
}

double DistanceHausdorff::reverse_max_box(VectorPoint2 img, VectorPoint2 elt, Box box) {

    double minimum = INF;
    double maximum = -1;
    for(auto i:elt){
        if(i.x>box.xMin && i.x<box.xMax && i.y>box.yMin && i.y<box.yMax){
            minimum = INF;
            for(auto j:img){
                double dist = glm::distance(i,j);
                if(dist<minimum) minimum=dist;
            }
            if(minimum>maximum) maximum=minimum;
        }
    }
    if (maximum == -1) return INF;
    return maximum;
}

double DistanceHausdorff::forward_reverse(double f, double r, int D) {
    switch(D){
        case 1: return std::min(f,r);
        case 2: return std::max(f,r);
        case 3: return 0.5 * (f+r);
        case 4: return -1; // ...
        default: return -1;
    }
}

void DistanceHausdorff::computeDeltaPrime(int w, int h, int level) {
    if(deltaPrims.size()>level){
        return;
    }
    std::vector<std::vector<double>> res;
    std::vector<double> vec;
    double min;
    int X = deltaXY.size();
    int Y = deltaXY[0].size();
    for(auto i=0; i<X; i++){
        vec.clear();
        for(auto j=0; j<Y; j++){
            min=INF;
            for(auto a = 0; a<=w; a++){
                for(auto b = 0; b<=h; b++){
                    if(a+i < X && b+j < Y){
                        min = std::min(min,deltaXY[i+a][j+b]);
                    }
                }
            }
            vec.push_back(min);
        }
        res.push_back(vec);
    }
    deltaPrims.push_back(res);
}

void DistanceHausdorff::popBackDeltaPrime() {
    deltaPrims.pop_back();
}

void DistanceHausdorff::cleanDeltaPrime() {
    deltaPrims.clear();
}