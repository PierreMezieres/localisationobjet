//
// Created by pierre on 27/12/18.
//

#include "Canny.hpp"

Mat canny(Mat img, int threshold){
    Mat res;
    res.create(img.rows, img.cols, img.type());
    cv::Canny(img, res,threshold,threshold*3);
    return res;
}


VectorPoint2 contours(Mat m)
{
    VectorPoint2 contours;
    for (int i=0 ; i < m.rows ; i++) {
        for (int j=0 ; j < m.cols ; j++) {
            if (m.at<uchar>(i,j) == 255) {
                contours.push_back(glm::vec2(i,j));
            }
        }
    }
    return contours;
}