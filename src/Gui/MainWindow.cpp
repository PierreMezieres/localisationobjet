#include <QtWidgets/QFileDialog>
#include "MainWindow.hpp"
#include "ui_mainwindow.h"
#include <Core/Macros.hpp>
#include <Log/Log.hpp>
#include <Core/Canny.hpp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //ui->comboBoxDirectedDistance->setCurrentIndex(4);
   // ui->comboBoxUndirectedDistance->setCurrentIndex(1);
    nbValue = 1;

    show_parameters(true,false,true,false,false,false);
    show_parametersBis(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_chargerImageButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(parentWidget(), QObject::tr("File loader"), "../../img", QObject::tr("File loader (*)"));
    if(filename != "") {
        cv::Mat image;
        image = cv::imread(filename.toStdString(), 0);   // Read the file

        if(! image.data )                              // Check for invalid input
        {
            Log(logError) <<  "Could not open or find the image";
            return;
        }
        search.setImage(filename.toStdString(),image);
        search.afficheImage();
    }


}

void MainWindow::on_chargerModeleButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(parentWidget(), QObject::tr("File loader"), "../../img", QObject::tr("File loader (*)"));
    if(filename != "") {
        cv::Mat image;
        image = cv::imread(filename.toStdString(), 0);   // Read the file

        if(! image.data )                              // Check for invalid input
        {
            Log(logError) <<  "Could not open or find the image";
            return;
        }
        search.setModel(image);
        search.afficheModel();
    }

}

void MainWindow::on_lancerRecherchceButton_clicked()
{
    search.launchSearch(D,d,nbValue);
    search.afficheResultat();
}

void MainWindow::on_AfficherResultatButton_clicked()
{
    search.afficheResultat();
}

void MainWindow::on_contourImageButton_clicked()
{
    search.afficheImageContour();
}

void MainWindow::on_contourModeleButton_clicked()
{
    search.afficheModelContour();
}

void MainWindow::on_afficherImageButton_clicked()
{
    search.afficheImage();
}

void MainWindow::on_afficherModeleButton_clicked()
{
    search.afficheModel();
}

void MainWindow::loadImage(std::string name) {

    search.setImage(name,cv::imread(name, 0));
    search.afficheImage();
    search.afficheImageContour();
}

void MainWindow::loadModel(std::string name) {
    search.setModel(cv::imread(name, 0));
    search.afficheModel();
    search.afficheModelContour();
}

void MainWindow::on_actionTest_1_triggered()
{
    loadImage("../../img/imgXerox.png");
    loadModel("../../img/imgXeroxMod.png");
}

void MainWindow::on_actionTest_3_triggered()
{
    loadImage("../../img/imgXerox.png");
    loadModel("../../img/xerox.jpg");
}

void MainWindow::on_actionTest_4_triggered()
{
    loadImage("../../img/imgXerox2.png");
    loadModel("../../img/imgXeroxMod.png");
}

void MainWindow::on_actionTest_5_triggered()
{
    loadImage("../../img/imgXerox3.png");
    loadModel("../../img/imgXeroxMod.png");
}

void MainWindow::on_actionTest_6_triggered()
{
    loadImage("../../img/dellImg1.jpg");
    loadModel("../../img/dellModel1-2-3.png");
}

void MainWindow::on_actionTest_7_triggered()
{
    loadImage("../../img/dellImg2.png");
    loadModel("../../img/dellModel1-2-3.png");
}


void MainWindow::on_actionTest_8_triggered()
{
    loadImage("../../img/dellImg3.png");
    loadModel("../../img/dellModel1-2-3.png");
}

void MainWindow::on_actionTest_9_triggered()
{
    loadImage("../../img/dellImg4.jpg");
    loadModel("../../img/dellModel.png");
}

void MainWindow::on_actionTest_10_triggered()
{
    loadImage("../../img/billabong.jpg");
    loadModel("../../img/billabongModel.png");
}



void MainWindow::on_comboBox_activated(int index) {
    search.setMethod(index);
    switch(index){
        case 0: show_parameters(true,false,true,false,false,false); break;
        case 1: show_parameters(true,false,false,false,false,false); break;
        case 2: show_parameters(false,false,true,false,false,false); break;
        case 3: show_parameters(true,true,true,true,false,false); break;
        case 4: show_parameters(true,true,false,false,true,true); break;
        case 5: show_parameters(false,false,true,true,true,true); break;
        default: show_parameters(true,true,true,true,true,true); break;
    }
}


void MainWindow::on_dminSpinBox_valueChanged(double arg1)
{
    search.setAreaMin(arg1);
}

void MainWindow::on_dmaxSpinBox_valueChanged(double arg1)
{
    search.setAreaMax(arg1);
}

void MainWindow::on_skewmaxSpinBox_valueChanged(double arg1)
{
    search.setSkewMax(arg1);
}

void MainWindow::on_smaxSpinBox_valueChanged(double arg1)
{
    search.setSMax(arg1);
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    search.setThresholdCannyModel(arg1);
    search.afficheModelContour();
}

void MainWindow::on_spinBox_2_valueChanged(int arg1)
{
    search.setThresholdCannyImage(arg1);
    search.afficheImageContour();
}

void MainWindow::on_tfSpinBox_valueChanged(double arg1)
{
    search.setTf(arg1);
}

void MainWindow::on_trSpinBox_valueChanged(double arg1)
{
    search.setTr(arg1);
}

void MainWindow::on_doubleSpinBox_8_valueChanged(double arg1)
{
    search.setFf(arg1);
}

void MainWindow::on_doubleSpinBox_7_valueChanged(double arg1)
{
    search.setFr(arg1);
}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{
    search.setpMul(arg1);
}

void MainWindow::on_doubleSpinBox_2_valueChanged(double arg1)
{
    search.setpPlu(arg1);
}

void MainWindow::show_parameters(bool tf, bool tr, bool ff, bool fr, bool pMul, bool pPlu){
    ui->tfSpinBox->setEnabled(tf);
    ui->trSpinBox->setEnabled(tr);
    ui->doubleSpinBox_8->setEnabled(ff);
    ui->doubleSpinBox_7->setEnabled(fr);
    ui->doubleSpinBox->setEnabled(pMul);
    ui->doubleSpinBox_2->setEnabled(pPlu);
    ui->label_6->setEnabled(tf);
    ui->label_7->setEnabled(tr);
    ui->label_8->setEnabled(ff);
    ui->label_9->setEnabled(fr);
    ui->label_15->setEnabled(pMul);
    ui->label_16->setEnabled(pPlu);
}

void MainWindow::show_parametersBis(bool b) {
    ui->dminSpinBox->setEnabled(b);
    ui->dmaxSpinBox->setEnabled(b);
    ui->skewmaxSpinBox->setEnabled(b);
    ui->smaxSpinBox->setEnabled(b);
    ui->label_2->setEnabled(b);
    ui->label_3->setEnabled(b);
    ui->label_4->setEnabled(b);
    ui->label_5->setEnabled(b);
    ui->label_17->setEnabled(b);
    ui->label_18->setEnabled(b);
    ui->label_19->setEnabled(b);
    ui->label_20->setEnabled(b);
    ui->label_21->setEnabled(b);
    ui->label_22->setEnabled(b);
    ui->label_23->setEnabled(b);
    ui->label_24->setEnabled(b);
    ui->aMinSpinBox->setEnabled(b);
    ui->aMaxSpinBox->setEnabled(b);
    ui->bMinSpinBox->setEnabled(b);
    ui->bMaxSpinBox->setEnabled(b);
    ui->cMinSpinBox->setEnabled(b);
    ui->cMaxSpinBox->setEnabled(b);
    ui->dMinSpinBox->setEnabled(b);
    ui->dMaxSpinBox->setEnabled(b);
    ui->comboBox_2->setEnabled(b);
}

void MainWindow::on_checkBox_toggled(bool checked)
{
    search.setTrans(checked);
    show_parametersBis(!checked);
}

void MainWindow::on_horizontalSlider_2_valueChanged(int value)
{

    search.setFactorResult(value/100.0);
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{

    search.setFactorImage(value/100.0);
}

void MainWindow::on_aMinSpinBox_valueChanged(double arg1)
{
    search.setAMin(arg1);
}

void MainWindow::on_bMinSpinBox_valueChanged(double arg1)
{
    search.setBMin(arg1);
}

void MainWindow::on_cMinSpinBox_valueChanged(double arg1)
{
    search.setCMin(arg1);
}

void MainWindow::on_dMinSpinBox_valueChanged(double arg1)
{
    search.setDMin(arg1);
}

void MainWindow::on_aMaxSpinBox_valueChanged(double arg1)
{
    search.setAMax(arg1);
}

void MainWindow::on_bMaxSpinBox_valueChanged(double arg1)
{
    search.setBMax(arg1);
}

void MainWindow::on_cMaxSpinBox_valueChanged(double arg1)
{
    search.setCMax(arg1);
}

void MainWindow::on_dMaxSpinBox_valueChanged(double arg1)
{
    search.setDMax(arg1);
}

void MainWindow::on_comboBox_2_activated(int index)
{
    switch(index){
        case 0: setMatrixRot(-1,1,-1,1,-1,1,-1,1); break;
        case 1: setMatrixRot(0.9,1,-0.05,0.05,-0.05,0.05,0.9,1);break;
    }

}


void MainWindow::setMatrixRot(double am, double aM, double bm, double bM, double cm, double cM, double dm, double dM){
    search.setAll(am,aM,bm,bM,cm,cM,dm,dM);

    ui->aMinSpinBox->setValue(am);
    ui->aMaxSpinBox->setValue(aM);
    ui->bMinSpinBox->setValue(bm);
    ui->bMaxSpinBox->setValue(bM);
    ui->cMinSpinBox->setValue(cm);
    ui->cMaxSpinBox->setValue(cM);
    ui->dMinSpinBox->setValue(dm);
    ui->dMaxSpinBox->setValue(dM);
}