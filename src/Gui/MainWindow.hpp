#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <Core/Search.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_comboBox_2_activated(int index);
    void on_dMaxSpinBox_valueChanged(double arg1);
    void on_cMaxSpinBox_valueChanged(double arg1);
    void on_bMaxSpinBox_valueChanged(double arg1);
    void on_aMaxSpinBox_valueChanged(double arg1);
    void on_dMinSpinBox_valueChanged(double arg1);
    void on_cMinSpinBox_valueChanged(double arg1);
    void on_bMinSpinBox_valueChanged(double arg1);
    void on_aMinSpinBox_valueChanged(double arg1);
    void on_actionTest_10_triggered();
    void on_actionTest_9_triggered();
    void on_actionTest_8_triggered();
    void on_horizontalSlider_valueChanged(int value);
    void on_horizontalSlider_2_valueChanged(int value);
    void on_checkBox_toggled(bool checked);
    void on_doubleSpinBox_2_valueChanged(double arg1);
    void on_doubleSpinBox_valueChanged(double arg1);
    void on_doubleSpinBox_7_valueChanged(double arg1);
    void on_doubleSpinBox_8_valueChanged(double arg1);
    void on_actionTest_7_triggered();
    void on_trSpinBox_valueChanged(double arg1);
    void on_tfSpinBox_valueChanged(double arg1);
    void on_spinBox_2_valueChanged(int arg1);
    void on_spinBox_valueChanged(int arg1);
    void on_smaxSpinBox_valueChanged(double arg1);
    void on_skewmaxSpinBox_valueChanged(double arg1);
    void on_dmaxSpinBox_valueChanged(double arg1);
    void on_dminSpinBox_valueChanged(double arg1);
    void on_comboBox_activated(int index);
    void on_actionTest_6_triggered();
    void on_actionTest_5_triggered();
    void on_actionTest_4_triggered();
    void on_actionTest_3_triggered();
    void on_actionTest_1_triggered();
    void on_afficherModeleButton_clicked();
    void on_afficherImageButton_clicked();
    void on_contourModeleButton_clicked();
    void on_contourImageButton_clicked();
    void on_AfficherResultatButton_clicked();
    void on_lancerRecherchceButton_clicked();
    void on_chargerModeleButton_clicked();
    void on_chargerImageButton_clicked();

private:
    Ui::MainWindow *ui;

    Search search;
    int d, D;
    int nbValue;
    void loadImage(std::string name);
    void loadModel(std::string name);

    void show_parameters(bool tf, bool tr, bool ff, bool fr, bool pMul, bool pPlu);
    void show_parametersBis(bool b);
    void setMatrixRot(double am, double aM, double bm, double bM, double cm, double cM, double dm, double dM);
};

#endif // MAINWINDOW_HPP
