# M2 project - Image analysis

Subject: Locating objects using Haussdorf distance

Students:
* Anna Laporte
* Pierre Mézières
* Suzanne Sorli

Supervisor:
* Alain Crouzil

Paul Sabatier University - 2018/2019

## How to compile

You'll need opencv installed on your system to compile !

```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
```

You'll find the *executable* in bin folder.

## User Manual

What you will find in the window

### Menu bar

- **Tests** : Load some preset tests

### Left of the window

- **Model** and **Image** : Load and display an image or the model that you want to find
- **Parameter Canny** : Choose the threshold for canny edge detector (image and model)
- **Launch Search** : The first combo box allows you to choose the research technique. You can launch the research and display the result.
Plus a cherry on the cake, you can enhance the visual result with 2 slide bars. 

(tf is the Threshold forward, the minimum distance used to compute the hausdorff distance. And ff, is the percentage of the contour point that meets the forward threshold. It's the same for the reverse criterion (tr and fr) )

**Research techniques**
- **F - first forward criterion** : The search is stopped when we find a transformation that meets the forward criterion.
- **F - best Fth forward criterion** : We find the best transformation that maximise ff (tf is set).
- **F - best Threshold forward criterion** : We wind the best transformation that maximise tf (ff is set).
- **F&R - first forward and reverse criterion** : The search is stopped when we find a transformation that meets the forward and the reverse criterion.
- **F&R - best Fth forward and reverse criterion** : We find the best transformation that maximise tf  and respect the reverse criterion (ff is set and fr is updated with *p\* * ff + p+*)
- **F&R - best Threshold forward and reverse criterion** : We find the best transformation that maximise ff and respect the reverse criterion (tf is set and tr is updated with *p\* * fr + p+*)


### Center of the window

- **Check box: only translation** : We want to only look at translations in the search space.
- **Spin box (Area min/Area max/skew max/s max)** : Restrict the search space.
- **Rest of the spin boxes** : Choose the differents parameters for the research techniques used.

### Right of the window

*Only for professionals* :sunglasses:

Allows you to restrict the search space by choosing the minimum and maximum rotation matrix. Rotation matrix is [a b; c d]